provider "helm" {
  kubernetes {
    host                   = data.aws_eks_cluster.my-cluster.endpoint
    cluster_ca_certificate = base64decode(data.aws_eks_cluster.my-cluster.certificate_authority.0.data)
    token                  = data.aws_eks_cluster_auth.my-auth.token
    load_config_file       = false
  }
}

resource "tls_private_key" "flux" {
  algorithm = "RSA"
  rsa_bits  = 4096
}

resource "kubernetes_secret" "flux-git-deploy" {
  metadata {
    name      = "flux-git-deploy"
    namespace = "kube-system"
  }

  type = "Opaque"

  data = {
    identity = tls_private_key.flux.private_key_pem
  }
}

resource "gitlab_deploy_key" "flux_deploy_key" {
  title    = "Flux"
  project  = "21699345"
  key      = tls_private_key.flux.public_key_openssh
  can_push = true
}

resource "helm_release" "helm-operator" {
  name             = "helm-operator"
  repository       = "https://charts.fluxcd.io/"
  chart            = "helm-operator"
  namespace        = helm_release.flux.namespace
  create_namespace = true
  set {
    name  = "helm.versions"
    value = "v3"
  }
  depends_on = [
    helm_release.flux,
  ]
}
resource "helm_release" "flux" {
  name       = "flux"
  repository = "https://charts.fluxcd.io/"
  chart      = "flux"
  namespace  = kubernetes_secret.flux-git-deploy.metadata[0].namespace
  set {
    name  = "git.url"
    value = "git@gitlab.com:emctl-gitops-demo/apps/cluster-management.git"
  }

  set {
    name  = "git.branch"
    value = "master"
  }

  set {
    name  = "git.path"
    value = "./helm"
  }

  set {
    name  = "git.secretName"
    value = kubernetes_secret.flux-git-deploy.metadata[0].name
  }

  set {
    name  = "helmOperator.create"
    value = "true"
  }

  set {
    name  = "rbac.create"
    value = "true"
  }

  set {
    name  = "syncGarbageCollection.enabled"
    value = "true"
  }

}
